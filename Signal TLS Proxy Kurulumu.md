# How to act as a proxy
This connection method is supported in the Signal Android app.

https://github.com/signalapp/Signal-TLS-Proxy

If you want to help by running a proxy, to get started you only need the following:

* A server with ports 80 and 443 available.
* A domain name (or subdomain) that points to the server’s IP address.

The proxy is extremely lightweight. An inexpensive and tiny VPS can easily handle hundreds of concurrent users. Here’s how to make it work:

1. SSH into the server.

2. Install Docker, Docker Compose, and git:
* `sudo apt update && sudo apt install docker docker-compose git`
3. Clone the Signal TLS Proxy repository:
* `git clone https://github.com/signalapp/Signal-TLS-Proxy.git`
4. Enter the repo directory:
* `cd Signal-TLS-Proxy`
5. Run the helper script that configures and provisions a TLS certificate from Let’s Encrypt:
* `sudo ./init-certificate.sh`
* You will be prompted to enter the domain or subdomain that is pointing to this server’s IP address.
6. Use Docker Compose to launch the proxy:
`sudo docker-compose up --detach`
Your proxy is now running! You can share your proxy with friends and family using this URL format: `https://signal.tube/#<your_domain_name>`

The Signal Android app is registered to handle links from `signal.tube`. The app can automatically configure proxy support when you tap on a link from any other app. This step happens before any web request is made, so even if a censor tries to block that domain it won’t accomplish anything. You can also manually configure proxy information in your Signal Settings too.

## An unorthodox-y proxy
Unlike a standard HTTP proxy, connections to the Signal TLS Proxy look just like regular encrypted web traffic. There’s no `CONNECT` method in a plaintext request to reveal to censors that a proxy is being used. Valid TLS certificates are provisioned for every proxy server, making it more difficult for censors to fingerprint the traffic than it would be if static self-signed certificates were used instead. In short, everything is designed to blend into the background as much as possible.

The Signal client establishes a normal TLS connection with the proxy, and the proxy simply forwards any bytes it receives to the actual Signal service. Any non-Signal traffic is blocked. Additionally, the Signal client still negotiates its standard TLS connection with the Signal endpoints through the tunnel.

This means that in addition to the end-to-end encryption that protects everything in Signal, all traffic remains opaque to the proxy operator.
